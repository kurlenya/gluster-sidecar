package main

import (
	"os"

	"github.com/spf13/pflag"
)

type KubeConfig struct {
	KubeMasterURL   string
	KubeUserName    string
	KubePassword    string
	KubeBearerToken string
	KubeCertFile    string
	KubeKeyFile     string
}

func NewKubeConfig() *KubeConfig {
	return &KubeConfig{
		KubeMasterURL:   os.Getenv("kube_master_url"),
		KubeUserName:    os.Getenv("kube_user_name"),
		KubePassword:    os.Getenv("kube_password"),
		KubeBearerToken: os.Getenv("kube_token"),
		KubeCertFile:    os.Getenv("kube_cert_file"),
		KubeKeyFile:     os.Getenv("kube_key_file"),
	}
}

func (s *KubeConfig) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&s.KubeMasterURL, "kube-master-url", s.KubeMasterURL, "URL to reach kubernetes master.")
	fs.StringVar(&s.KubeUserName, "kube-user-name", s.KubeMasterURL, "UserName to reach kubernetes master.")
	fs.StringVar(&s.KubePassword, "kube-password", s.KubePassword, "Password to reach kubernetes master.")
	fs.StringVar(&s.KubeBearerToken, "kube-token", s.KubeBearerToken, "Token to reach kubernetes master.")
	fs.StringVar(&s.KubeCertFile, "kube-cert-file", s.KubeCertFile, "CertFile to reach kubernetes master.")
	fs.StringVar(&s.KubeKeyFile, "kube-key-file", s.KubeKeyFile, "KeyFile to reach kubernetes master.")
}
