package main

import (
	"errors"
	"log"
	"net/http"
	"time"

	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"github.com/spf13/pflag"
	"k8s.io/kubernetes/pkg/api"
	"k8s.io/kubernetes/pkg/client/cache"
	"k8s.io/kubernetes/pkg/client/restclient"
	client "k8s.io/kubernetes/pkg/client/unversioned"
	"k8s.io/kubernetes/pkg/controller/framework"
	"k8s.io/kubernetes/pkg/fields"
	"k8s.io/kubernetes/pkg/labels"
	"k8s.io/kubernetes/pkg/util/wait"
)

const (
	zone = 1
)

var diskDevice = os.Getenv("DISK_DEVICE")
var currentNamespace = os.Getenv("NAMESPACE")
var listenPort = ":" + os.Getenv("PORT")

type hostnames struct {
	Manage  []string
	Storage []string
}

type node struct {
	Hostnames hostnames
	Zone      int
}

type nodeDefinition struct {
	Node    node
	Devices []string
}

type cluster struct {
	Nodes []nodeDefinition
}

type clustersList struct {
	Clusters []cluster
}

func setLabel(obj interface{}, client *client.Client) {
	pod := obj.(*api.Pod)
	if labelVal, ok := pod.Labels["glusterfs-node"]; !ok || labelVal != pod.Status.HostIP {
		pod.Labels["glusterfs-node"] = pod.Status.HostIP
		_, err := client.Pods(pod.Namespace).Update(pod)
		if err != nil {
			Error.Printf("Error update label on pod %s: %s\n", pod.Name, err)
		} else {
			Info.Println("Add label glusterfs-node to", pod.Name, " :", pod.Labels["glusterfs-node"])
		}
	} else {
		Info.Printf("No need to update %s labels\n", pod.Status.HostIP)
	}
}

func generatedNodeDefinition(obj interface{}) nodeDefinition {
	pod := obj.(api.Pod)
	node := nodeDefinition{}
	node.Node.Hostnames.Manage = []string{pod.Status.PodIP}
	node.Node.Hostnames.Storage = []string{pod.Status.PodIP}
	node.Node.Zone = zone
	node.Devices = []string{diskDevice}
	return node
}

func generateTopology(namespace string, client *client.Client) (string, error) {
	selector := labels.Set{"glusterfs": "pod"}.AsSelector()
	options := api.ListOptions{LabelSelector: selector}
	podList, _ := client.Pods(namespace).List(options)
	glusterFsCluster := cluster{}
	for _, pod := range podList.Items {
		if labelVal, ok := pod.Labels["glusterfs-node"]; ok && labelVal == pod.Status.HostIP {
			glusterFsCluster.Nodes = append(glusterFsCluster.Nodes, generatedNodeDefinition(pod))
		}
	}
	if len(glusterFsCluster.Nodes) < len(podList.Items) {
		return "", errors.New("Not all pods are labeled. Still waiting.")
	} else {
		topology := clustersList{}
		topology.Clusters = append(topology.Clusters, glusterFsCluster)
		topologyJson, _ := json.Marshal(topology)
		topologyFile, _ := ioutil.TempFile(os.TempDir(), "glusterfstopology")
		f, _ := os.OpenFile(topologyFile.Name(), os.O_RDWR|os.O_CREATE, 0666)
		f.Write(topologyJson)
		f.Close()
		return string(topologyFile.Name()), nil
	}
}

func updateTopology(obj interface{}, client *client.Client) {
	pod := obj.(*api.Pod)
	if labelVal, _ := pod.Labels["glusterfs"]; labelVal == "pod" {
		status := checkPodState(obj)
		if status == "Ready" {
			Info.Printf("Found pod '%s' on %s with label '%s'\n", pod.Name, pod.Status.HostIP, pod.Labels["glusterfs"])
			setLabel(obj, client)
			topology, err := generateTopology(obj.(*api.Pod).Namespace, client)
			if err != nil {
				Warning.Println(err)
			} else {
				Info.Printf("Save new topology file in %s\n", topology)
				applyTopology(topology)
			}
		} else {
			Warning.Printf("Found pod '%s' on %s, still wait for pod readiness", pod.Name, pod.Status.HostIP)
		}
	}
}

func applyTopology(topology string) {
	app := "/usr/bin/heketi-cli"
	jsonFile := "--json=" + topology

	cmd := exec.Command(app, "topology", "load", jsonFile)
	Info.Printf("Applying new topology from %s\n", topology)
	output, err := cmd.CombinedOutput()
	if err != nil {
		Error.Println(fmt.Sprint(err) + ": " + string(output))
		return
	} else {
		Info.Printf("%s\n", string(output))
	}
	defer os.Remove(topology)

}

func checkPodState(obj interface{}) string {
	pod := obj.(*api.Pod)
	var nonReadyContainers int
	nonReadyContainers = 0
	for _, container := range pod.Status.ContainerStatuses {
		if !container.Ready {
			nonReadyContainers++
		}
	}
	if nonReadyContainers == 0 && pod.Status.Phase == api.PodRunning {
		return "Ready"
	} else {
		return "Non-ready"
	}

}

func watchServices(client *client.Client, store cache.Store) cache.Store {
	//Define what we want to look for (Pods)
	watchlist := cache.NewListWatchFromClient(client, "pods", currentNamespace, fields.Everything())
	resyncPeriod := 30 * time.Minute
	//Setup an informer to call functions when the watchlist changes
	eStore, eController := framework.NewInformer(
		watchlist,
		&api.Pod{},
		resyncPeriod,
		framework.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				updateTopology(obj, client)
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				pod := newObj.(*api.Pod)
				if labelVal, ok := pod.Labels["glusterfs-node"]; !ok || labelVal != pod.Status.HostIP {
					updateTopology(newObj, client)
				}
			},
			DeleteFunc: func(obj interface{}) {
				pod := obj.(*api.Pod)
				fmt.Println("Delete is", pod.Name)
			},
		},
	)
	//Run the controller as a goroutine
	go eController.Run(wait.NeverStop)
	return eStore
}

func main() {

	Init(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)

	kubeConfig := NewKubeConfig()
	kubeConfig.AddFlags(pflag.CommandLine)

	pflag.Parse()

	//Configure cluster info
	var (
		config *restclient.Config
		err    error
	)

	if kubeConfig.KubeMasterURL != "" && kubeConfig.KubeBearerToken != "" {
		config = &restclient.Config{
			Host:        kubeConfig.KubeMasterURL,
			BearerToken: kubeConfig.KubeBearerToken,
			Insecure:    true,
		}
	} else if kubeConfig.KubeUserName != "" && kubeConfig.KubePassword != "" {
		config = &restclient.Config{
			Host:     kubeConfig.KubeMasterURL,
			Username: kubeConfig.KubeUserName,
			Password: kubeConfig.KubePassword,
			Insecure: true,
		}
	} else if kubeConfig.KubeCertFile != "" && kubeConfig.KubeKeyFile != "" {
		config = &restclient.Config{
			Host:     kubeConfig.KubeMasterURL,
			Insecure: true,
			TLSClientConfig: *&restclient.TLSClientConfig{
				CertFile: kubeConfig.KubeCertFile,
				KeyFile:  kubeConfig.KubeKeyFile,
			},
		}
	} else {
		config, err = restclient.InClusterConfig()
		if err != nil {
			log.Fatalln("Kube Client Config not created sucessfully:", err)
		}
	}

	//Create a new client to interact with cluster and freak if it doesn't work
	kubeClient, err := client.New(config)
	if err != nil {
		log.Fatalln("Kube Client not created sucessfully:", err)
	}

	//Create a cache to store Pods
	var podsStore cache.Store
	//Watch for Pods
	podsStore = watchServices(kubeClient, podsStore)
	//Keep alive
	log.Fatal(http.ListenAndServe(listenPort, nil))
}
