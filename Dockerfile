FROM golang:1.7

ENV wd "${GOPATH}/src/gitlab.com/kurlenya/gluster-sidecar"

ENV HEKETI_CLI_SERVER http://localhost:8080
ENV DISK_DEVICE /dev/xvdb
ENV PORT 8181
ENV HEKETI_CLI_VERSION v3.1.0
ENV NAMESPACE default

RUN apt-get update
RUN apt-get install -y wget curl

WORKDIR $wd

ADD . $wd

RUN go install

WORKDIR /

RUN wget --quiet https://github.com/heketi/heketi/releases/download/${HEKETI_CLI_VERSION}/heketi-client-${HEKETI_CLI_VERSION}-HEAD.linux.amd64.tar.gz
RUN tar -zxvf heketi-client-${HEKETI_CLI_VERSION}-HEAD.linux.amd64.tar.gz
RUN mv heketi-client/bin/heketi-cli /usr/bin/heketi-cli
RUN chmod +x /usr/bin/heketi-cli

RUN rm -R $wd

ENTRYPOINT gluster-sidecar